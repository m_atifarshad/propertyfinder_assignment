/**
 * Created by Mohammad Atif on 21-Oct-16.
 */

(function(global, jQ){
    'use strict';

    var default_settings;


    //variable assignments
    default_settings = {
        loop:true,
        autoplay:true,
        margin:10,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:6
            }
        }
    };

    // assumption: will accept only ID as selector
    var Carousel = function(selector){

        //get prefix of the selector
        //only id is acceptable

        var prefix = selector.trim().substr(0,1);

        if(prefix === '#'){
            selector = selector.trim().substr(1,selector.length);

            if(document.getElementById(selector)){
                return new Carousel.constructor(selector);
            }
            else
                throw 'No Such ID exist';
        }
        else{
            throw 'Invalid ID';
        }

    };


    Carousel.prototype = {

        // initialise the current carousel
        init: function (settings) {
            this.default_settings = settings || default_settings;
            $('#'+this.selector).owlCarousel(this.default_settings);
        },

        //populate images into carousel
        //accepts array of data
        populateLogos: function (imageList, callback) {
            var count = imageList.length -1;

            var item, span, img;

            while(count > 0 ){

                item = createElement('div', 'item');
                item = document.getElementById(this.selector).appendChild(item);

                span = createElement('span', 'logoWrapper');
                span = item.appendChild(span);

                img = createElement('img');
                img.src = imageList[count];
                span.appendChild(img);

                count --;

            }


            if(callback && typeof(callback) === "function")
                callback();
            else
                throw "invalid callback";
        }
    };

    Carousel.constructor = function (selector) {
        var self = this;
        self.selector = selector || '';
    };


    //private functions


    //create new elements for carousel list
    // principally should exist serperately not inside carousel
    // creating inside carousel for a quick short hand function for creating new tags

    function createElement(tagName, className) {
        var el = document.createElement(tagName);

        //default className is item for all carousel items
        // can further be revised to better approach
        el.className = className || '';

        return el;
    }


    //creating global scope
    Carousel.constructor.prototype = Carousel.prototype;
    global.pfCoursel = global.$pfCoursel = Carousel;



}(window, jQuery));

