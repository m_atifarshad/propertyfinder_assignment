/**
 * Created by Mohammad Atif on 19-Oct-16.
 */


(function (global, jQ) {

    'use strict';

    //set defaults
    var lang = ($('html').attr('lang') || 'en').trim().toLowerCase();

    //logo carousel
    var logo_carousel = $pfCoursel('#pf-carousel');
    
    //using a local json
    //to parse the json from propertyfinder.ae we need a local server as browsers follow same domain policy
    //server options nodejs, apache, tomcat or iis to parse the json
    //I have included it directly, we can directly pass the json link on same domain
    // or alternately, if we need to communicate with cross domain json, then we can add the domain in the trusted list on the server to communicate
    
    var data_url = 'json/data.json';
    
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": data_url,
        "method": "GET",
        "headers": {
            "cache-control": "no-cache"
        }
    };

    $.ajax(settings)
        .done(function (response) {
            
            var data = response.data;
            var logoImages = brokers.getSmallLogos(data);
    
    
            logo_carousel.populateLogos(logoImages, function(){
    
                //initialize logo carousel once the data is populated in the dom
                logo_carousel.init();
            });
    
        })
        
        .fail(function(response){
            alert('failed to load data');
        });



    var brokers = (function () {


        function getSmallLogos(jsonData){
            var logos = [];

            //console.log(jsonData)

            $.each(jsonData,function(key, value){
                logos.push(value.links.logo);
            });

            return logos;
        }

        return{
            getSmallLogos: getSmallLogos
        }
    }());


})(window, jQuery);


