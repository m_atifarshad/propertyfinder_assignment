# PropertyFinder Assignment

This is a PropertyFinder Assignment for frontend evaluation based on 
* [nodejs](http://nodejs.org/).
* [bootstrap](http://getbootstrap.com/).
* [jQuery](http://jquery.com/).

## Getting Started

To get you started you can simply clone the repository and install the dependencies:

### Prerequisites

You need git to clone the repository. You can get git from
[http://git-scm.com/](http://git-scm.com/).

I have also used a number of node.js and its tools for the assignment. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).


### Clone PropertyFinder Assignment

Clone the repository using [git][git]:


### Install Dependencies

I have two kinds of dependencies in this project:

* tools for managing the project (development and build releases) . I depend upon it via `npm`, the [node package manager][npm].
* other frontend dependencies via `bower`, a [client-side code package manager][bower]. This is use for frontend dependancies

I have preconfigured `npm` to automatically install `bower` globally, go to the root directory via CLI and type:

```
npm install
```

This will also call `npm install -g bower`.  You should find that you have a new
folder in your project.

* `node_modules` - contains dependancies for gulp task automation

Then go inside the `source` folder and type:

```
bower install
```

Once installed, you should find that you have a new folder now inside the `source` directory with the name `bower_components`. This folder contains all the
frontend libraries required for your project

### Run the Application

Go to your root directory 

```
gulp build
```


Once the tasks are executed, this will create the `build` folder and put the compiled files in `build` folder 
and once the compilation is done you should see something like this on your screen

```
 ------------------------------------
       Local: http://localhost:3005
    External: http://192.168.1.6:3005
 ------------------------------------
          UI: http://localhost:3001
 UI External: http://192.168.1.6:3001
 ------------------------------------

```

Now browse to the home page at `http://localhost:3005/`.
For other handheld devices or other pc's on the same network you can use the generated External IP to view the live site.

*Note this will start the application from the build folder. Attach is the screen shot of the home page
![Home page screen shot](screenshots/property_finder_carousel_desktop.png)
![Home page screen shot Mobile](screenshots/property_finder_carousel_mobile.png)


## Production Build
All js, css files will be minified and concatinated with their respective tear if required. javascript file(s) will be encrypted and
local images will be compressed.

### Windows
To make a production build on windows all you have to do is set the enviornmental variable NODE_ENV to production. Open
the command prompt by typing cmd in the run panel or by browsing it through program applications and


```
SET NODE_ENV=production
```

once you set the `NODE_ENV` to production, you can build the project using the same gulp commad as above

```
gulp build
```

This will minify the html, css, js files along with compression of images